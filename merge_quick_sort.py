from typing import List
import numpy as np
import time
import random
from Algorithm.sort import Sort

"""
归并排序:分而治之，逐步合并
"""


def __merge(pre, post):
    a = []
    i, j = 0, 0
    while i < len(pre) and j < len(post):
        if pre[i] <= post[j]:
            a.append(pre[i])
            i += 1
        else:
            a.append(post[j])
            j += 1
    # if i == left:
    #     for k in range(j, right):
    #         a.append(post[k])
    # else:
    #     for k in range(i, left):
    #         a.append(pre[k])
    if i == len(pre):
        for n in post[j:]:
            a.append(n)
    else:
        for n in pre[i:]:
            a.append(n)
    return a


def merge_sort(a: List[int]):
    if len(a) > 1:
        mid = len(a) // 2
        pre = merge_sort(a[:mid])
        post = merge_sort(a[mid:])
        return __merge(pre, post)
    else:
        return a


def __partition(a: List[int]):
    """
    选中一个基准点，将元素与基准点做对比，大的往后，小的往前
    :param a:
    :return:
    """
    pivot, p_index = a[0], 0
    for i in range(1, len(a)):
        if a[i] <= pivot:
            a[i], a[p_index + 1] = a[p_index + 1], a[i]
            p_index += 1
    a[p_index], a[0] = a[0], a[p_index]
    return p_index


def quick_sort(a: List[int]):
    p_index = __partition(a)
    if len(a) <= 2 or (len(a) == 3 and p_index == 1):
        return a
    elif p_index in (len(a)-1, len(a)-2):   #基准点是最后两个元素，基准点后的元素不需要再次排序
        return quick_sort(a[:p_index]) + a[p_index:]
    elif p_index in (0, 1):
        return a[:p_index+1] + quick_sort(a[p_index+1:])
    else:
        return quick_sort(a[:p_index]) + [a[p_index]] + quick_sort(a[p_index+1:])


def __partition_random_p(a: List[int]):
    # 获取一个随机元素作为基准数
    long = len(a)
    k = random.randint(0, long-1)
    a[0], a[k] = a[k], a[0]
    # 排序
    pivot, p_index = a[0], 0
    for i in range(1, len(a)):
        if a[i] <= pivot:
            a[i], a[p_index + 1] = a[p_index + 1], a[i]
            p_index += 1
    a[p_index], a[0] = a[0], a[p_index]
    return p_index


def quick_sort_random_p(a: List[int]):
    p_index = __partition_random_p(a)
    if len(a) <= 2 or (len(a) == 3 and p_index == 1):
        return a
    elif p_index in (len(a)-1, len(a)-2):
        return quick_sort_random_p(a[:p_index]) + a[p_index:]
    elif p_index in (0, 1):
        return a[:p_index+1] + quick_sort_random_p(a[p_index+1:])
    else:
        return quick_sort_random_p(a[:p_index]) + [a[p_index]] + quick_sort_random_p(a[p_index+1:])


def __partition_both_side_random(a: List[int]):
    length = len(a)
    # 获取基准数
    k = random.randint(0, len(a)-1)
    a[0], a[k] = a[k], a[0]

    #区分
    i, j, pivot = 1, length-1, a[0]
    while i < j:
        while i < length and a[i] < pivot:
            i += 1
        while j > i and a[j] > pivot:
            j -= 1
        a[i], a[j] = a[j], a[i]
    a[j-1], a[0] = a[0], a[j-1]
    return j-1


def quick_both_side_random_p(a: List[int]):
    p_index = __partition_both_side_random(a)
    if len(a) <= 2 or (len(a) == 3 and p_index == 1):
        return a
    elif p_index in (len(a)-1, len(a)-2):   #基准点是最后两个元素，基准点后的元素不需要再次排序
        return quick_sort(a[:p_index]) + a[p_index:]
    elif p_index in (0, 1):
        return a[:p_index+1] + quick_sort(a[p_index+1:])
    else:
        return quick_sort(a[:p_index]) + [a[p_index]] + quick_sort(a[p_index+1:])


def quick_sort_insertion_optimize(a: List[int]):
    length = len(a)
    if length > 4:
        p_index = __partition_random_p(a)
        if len(a) <= 2 or (len(a) == 3 and p_index == 1):
            return a
        elif p_index in (len(a) - 1, len(a) - 2):
            return quick_sort_random_p(a[:p_index]) + a[p_index:]
        elif p_index in (0, 1):
            return a[:p_index + 1] + quick_sort_random_p(a[p_index + 1:])
        else:
            return quick_sort_random_p(a[:p_index]) + [a[p_index]] + quick_sort_random_p(a[p_index + 1:])
    else:
        Sort.insertion_sort(a)


def __partition_both_side(a: List[int]):
    i, j, pivot = 1, len(a) - 1, a[0]
    while i < j:
        while i < len(a) and a[i] < pivot:
            i += 1
        while j > i and a[j] > pivot:
            j -= 1
        a[i], a[j] = a[j], a[i]
    a[j-1], a[0] = a[0], a[j-1]
    return j-1


def quick_sort_both_side(a: List[int]):
    p_index = __partition_both_side(a)
    if len(a) <= 2 or (len(a) == 3 and p_index == 1):
        return a
    elif p_index in (len(a)-1, len(a)-2):   #基准点是最后两个元素，基准点后的元素不需要再次排序
        return quick_sort(a[:p_index]) + a[p_index:]
    elif p_index in (0, 1):
        return a[:p_index+1] + quick_sort(a[p_index+1:])
    else:
        return quick_sort(a[:p_index]) + [a[p_index]] + quick_sort(a[p_index+1:])


def test():
    array = [5, 6, -1, 4, 2, 8, 10, 7, 6]
    a = list(np.random.randint(10000, size=10000))
    n = a.copy()
    m = a.copy()
    p, s, q = a.copy(), a.copy(), a.copy()
    once = time.time()
    print(merge_sort(a))
    twice = time.time()
    print(quick_sort(n))
    third = time.time()
    print(quick_sort_random_p(m))
    fourth = time.time()
    print(quick_both_side_random_p(p))
    fifth = time.time()
    print(quick_sort_insertion_optimize(s))
    sixth = time.time()
    print(quick_sort_both_side(q))
    seventh = time.time()
    print('归并时间：', twice - once)
    print('快排时间：', third - twice)
    print('随机基准值的快排时间：', fourth - third)
    print('随机基准值双向的快排时间：', fifth - fourth)
    print('优化的快排时间：', sixth - fifth)
    print('双向的快排时间：', seventh - sixth)


if __name__ == "__main__":
    test()
