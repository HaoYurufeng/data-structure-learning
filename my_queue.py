from typing import Optional
from itertools import chain


class Node:
    def __init__(self, data, next_node=None):
        self.__data = data
        self.__next = next_node

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, data):
        self.__data = data

    @property
    def next_node(self):
        return self.__next

    @next_node.setter
    def next_node(self, next_node):
        self.__next = next_node


class ArrayQueue:
    """
    Queue based upon array
    用数组实现队列
    """
    def __init__(self, capacity: int):
        self._items = []
        self._capacity = capacity
        self._head = 0
        self._tail = 0

    def enqueue(self, item: str) -> bool:
        if self._tail == self._capacity:     #队尾满
            if self._head == 0:              #对头满
                return False
            else:                            #剩余元素整体往前挪位置
                for i in range(0, self._tail - self._head):
                    self._items[i] = self._items[i + self._head]
                self._tail = self._tail - self._head
                self._head = 0

        self._items.insert(self._tail, item)
        self._tail += 1
        return True

    def dequeue(self) -> Optional[str]:
        if self._head != self._tail:
            item = self._items[self._head]
            self._head += 1
            return item
        else:
            return None

    def __len__(self):
        return self._tail - self._head

    def __repr__(self):
        return " ".join(item for item in self._items[self._head: self._tail])


class LinkedQueue:
    def __init__(self):
        self.__head: Optional[Node] = None
        self.__tail: Optional[Node] = None

    def enqueue(self, value):
        new_node = Node(value)
        if self.__tail:
            self.__tail.next_node = new_node
        else:
            self.__head = new_node
        self.__tail = new_node

    def dequeue(self):
        if self.__head:
            value = self.__head.data
            self.__head = self.__head.next_node
            if not self.__head:
                self.__tail = None
            return value

    def len(self):
        p = self.__head
        length = 0
        while p:
            p = p.__next
            length += 1
        return length

    def __repr__(self):
        p = self.__head
        values = []
        while p:
            values.append(p.data)
            p = p.next_node
        return ' ->'.join(value for value in values)


class CircularQueue:
    def __init__(self, capacity):
        self.__items = []
        self.__capacity = capacity + 1
        self.__head = 0
        self.__tail = 0

    def enqueue(self, item):
        if (self.__tail + 1) % self.__capacity == self.__head:
            return False
        self.__items.append(item)
        self.__tail = (self.__tail + 1) % self.__capacity
        return True

    def dequeue(self):
        if self.__head != self.__tail:
            item = self.__items[self.__head]
            self.__head = (self.__head + 1) % self.__capacity
            return item

    def __repr__(self):
        if self.__tail >= self.__head:
            return " ".join(item for item in self.__items[self.__head:self.__tail])
        else:
            return " ".join(item for item in chain(self.__items[self.__head:], self.__items[:self.__tail]))


def test():
    print("---------------基于数组实现队列---------------")
    a_queue = ArrayQueue(5)
    for i in range(0, 9):
        a_queue.enqueue(str(i))
    print(a_queue)
    print(len(a_queue))
    print(a_queue.dequeue())
    print(a_queue)
    a_queue.enqueue('8')
    print(a_queue)
    print("---------------基于链表实现队列---------------")
    l_queue = LinkedQueue()
    for i in range(10):
        l_queue.enqueue(str(i))
    print(l_queue)
    for _ in range(3):
        l_queue.dequeue()
    print(l_queue)

    l_queue.enqueue("7")
    l_queue.enqueue('8')
    print(l_queue)
    l_queue.dequeue()
    print(l_queue)

    print("---------------基于数组实现环状队列---------------")
    c_queue = CircularQueue(5)
    for i in range(5):
        c_queue.enqueue(str(i))
    print(c_queue)
    c_queue.dequeue()
    c_queue.dequeue()
    print(c_queue)
    c_queue.enqueue(str(5))
    print(c_queue)


if __name__ == "__main__":
    test()
