"""
排序算法：冒泡、插入、选择
均为基于比较的算法
注意三个问腿：1、算是原地排序算法吗？
            2、是稳定的排序算法吗？
            3、时间复杂度是多少？
注意两个概念：逆序度、有序度、满有序度
1、原地排序，紧占用常数个临时空间，空间复杂度为O(1)
2、是否稳定，需要考虑相同大小的元素在排序前后是否发生顺序改变
    冒泡排序和插入排序只有在大小不同时才会发生交换和插入操作，所以是稳定的
    但选择排序，eg:a = [5,8,5,2,9],第一次查找最小数时，a[0]与a[3]发生交换，两个5的前后顺序已经发生改变
3、时间复杂度：均为O(n**2)
    冒泡和插入排序最好情况为O(n),最差为O(n**2)
    选择排序均为O(n**2)
4、冒泡、插入，优选插入：每次交换花费K时间，Java需要三次赋值，python两次赋值，时间为2K-3K
                    插入移动操作，只需要K
"""

from typing import List
import time
import copy
import numpy as np


def bubble_sort(a: List[int]):
    start_time = time.time()
    length = len(a)
    if length > 1:
        for i in range(length):
            made_swap = False
            for j in range(length-i-1):
                if a[j] > a[j+1]:
                    a[j], a[j+1] = a[j+1], a[j]
                    made_swap = True
            if not made_swap:
                break
    end_time = time.time()
    print('运行时间：', (end_time - start_time)*10000)


def bubble_sort2(a: List[int]):
    start_time = time.time()
    length = len(a)
    if length > 1:
        for i in range(length):
            for j in range(length-i-1):
                if a[j] > a[j+1]:
                    a[j], a[j+1] = a[j+1], a[j]
    end_time = time.time()
    print('2运行时间：', (end_time - start_time)*10000)


def bubble_sort3(a: List[int]):
    start_time = time.time()
    length = len(a)
    for i in range(length - 1, 0, -1):
        for j in range(0, i):
            if a[j] > a[j + 1]:
                a[j], a[j + 1] = a[j + 1], a[j]
    end_time = time.time()
    print('3运行时间：', (end_time - start_time) * 10000)


def insertion_sort(a: List[int]):
    """
    思路（升序）：按顺序遍历每一个元素，与自己之前的元素做比较,若小，则插入元素，所有元素往后移动
    :param a:
    :return:
    """
    start_time = time.time()
    length = len(a)
    if length > 1:
        for i in range(1, length):
            value = a[i]
            j = i - 1
            while j >= 0 and a[j] > value:
                a[j+1] = a[j]
                j -= 1
            a[j+1] = value
    end_time = time.time()
    print('插入排序运行时间：', (end_time - start_time) * 10000)


def selection_sort(a: List[int]):
    """
    与插入排序的区别在于，它找到剩下无序元素的最大或最小，与无序元素的第一个比较，得到第n个最值，然后交换位置
    比较适合大部分元素有序的数组
    :param a:
    :return:
    """
    start_time = time.time()
    length = len(a)
    if length > 1:
        for i in range(length):
            min_index = i
            min_value = a[i]
            for j in range(i+1, length):
                if a[j] < min_value:
                    min_value = a[j]
                    min_index = j
            a[i], a[min_index] = a[min_index], a[i]
    end_time = time.time()
    print('选择排序运行时间：', (end_time - start_time) * 10000)


def test_bubble_sort():
    test_array = [1, 1, 1, 1]
    bubble_sort(test_array)
    assert test_array == [1, 1, 1, 1]
    test_array = [4, 1, 2, 3]
    bubble_sort(test_array)
    assert test_array == [1, 2, 3, 4]
    test_array = [4, 3, 2, 1]
    bubble_sort(test_array)
    assert test_array == [1, 2, 3, 4]


def test_insertion_sort():
    test_array = [1, 1, 1, 1]
    insertion_sort(test_array)
    assert test_array == [1, 1, 1, 1]
    test_array = [4, 1, 2, 3]
    insertion_sort(test_array)
    assert test_array == [1, 2, 3, 4]
    test_array = [4, 3, 2, 1]
    insertion_sort(test_array)
    assert test_array == [1, 2, 3, 4]


def test_selection_sort():
    test_array = [1, 1, 1, 1]
    selection_sort(test_array)
    assert test_array == [1, 1, 1, 1]
    test_array = [4, 1, 2, 3]
    selection_sort(test_array)
    assert test_array == [1, 2, 3, 4]
    test_array = [4, 3, 2, 1]
    selection_sort(test_array)
    assert test_array == [1, 2, 3, 4]


if __name__ == "__main__":
    a = np.random.randint(0, 1000, 1000)
    array = [5, 6, -1, 4, 2, 8, 10, 7, 6]
    selection_sort(array)
    print(array)
    print(a)
    array1 = copy.deepcopy(a)
    array2 = copy.deepcopy(a)
    array3 = copy.deepcopy(a)
    array4 = copy.deepcopy(a)
    array5 = copy.deepcopy(a)
    bubble_sort(array1)
    bubble_sort3(array2)
    bubble_sort2(array3)
    insertion_sort(array4)
    selection_sort(array5)
    # print(array1)
    # print(array2)
    # print(array3)
    # print(array4)
    # print(array5)
    test_bubble_sort()
    test_insertion_sort()
    test_selection_sort()
