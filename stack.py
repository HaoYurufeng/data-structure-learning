"""
Stack based upon linked list
基于链表实现栈
"""

from typing import Optional

class Node:
    def __init__(self, data: int, next_node=None):
        self._data = data
        self._next = next_node

class LinkedStack:
    def __init__(self):
        self._top: Node = None

    def push(self, value: int):
        new_top = Node(value)
        new_top._next = self._top
        self._top = new_top

    def pop(self) -> Optional[int]:
        if self._top:
            value = self._top._data
            self._top = self._top._next
            return value

    def len(self):
        length = 0
        p = self._top
        while p:
            p = p._next
            length += 1
        return length

    def __repr__(self) -> str:
        current = self._top
        nums = []
        while current:
            nums.append(current._data)
            current = current._next
        return " ".join(f"{num}" for num in nums)


if __name__ == "__main__":
    stack = LinkedStack()
    print(stack.len())
    for i in range(9):
        stack.push(i)
    print(stack)
    print(stack.len())
    for _ in range(3):
        stack.pop()
    print(stack)
    print(stack.len())
