# To Learn:
# 1) Insertion, deletion and random access of array
# 2) Assumes int for element type
#
# Author:
#


class MyArray:
    def __init__(self, capacity: int):
        self.__data = []
        self.__capacity = capacity

    def __getitem__(self, position: int) -> object:
        return self.__data[position]

    def __setitem__(self, index: int, value: object):
        self.__data[index] = value

    def __iter__(self):
        for item in self.__data:
            yield item

    def __len__(self) -> int:
        return len(self.__data)

    def find(self, index: int) -> object:
        try:
            return self.__data[index]
        except IndexError:
            return None

    def delete(self, index: int) -> bool:
        try:
            self.__data.pop(index)
            return True
        except IndexError:
            return False

    def insert(self, index: int, value: object) -> bool:
        if len(self.__data) >= self.__capacity:
            return False
        else:
            self.__data.insert(index, value)
            return True

    def print_all(self):
        for item in self.__data:
            print(item, end=' ')


def test_my_array():
    array = MyArray(5)
    array.insert(0, 3)
    array.insert(0, 4)
    array.insert(1, 5)
    array.insert(3, 9)
    array.print_all()
    print()
    array.insert(3, 10)
    assert array.insert(0, 100) is False
    assert len(array) == 5
    assert array.find(1) == 5
    array.print_all()
    assert array.delete(4) is True
    print()
    array.print_all()


if __name__ == "__main__":
    test_my_array()
