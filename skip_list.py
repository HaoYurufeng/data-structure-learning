"""
跳表
"""

from typing import Optional, List
import random


class ListNode:
    def __init__(self, data: Optional[int] = None):
        self.__data = data
        self.__forwards = []

    @property
    def data(self):
        return self.__data

    @data.setter
    def data(self, data):
        self.__data = data

    @property
    def forwards(self):
        return self.__forwards

    @forwards.setter
    def forwards(self, forwards: List):
        self.__forwards = forwards


class SkipList:
    _MAX_LEVEL = 16

    def __init__(self):
        self._level_count = 1
        self._head = ListNode()
        self._head.forwards = [None] * type(self)._MAX_LEVEL

    def find(self, value: int) -> Optional[ListNode]:
        p = self._head
        for i in range(self._level_count - 1, -1, -1):
            while p.forwards[i] and p.forwards[i].data < value:
                p = p.forwards[i]
        return p.forwards[0] if p.forwards[0] and p.forwards[0].data == value else None

    def insert(self, value: int):
        level = self.__random_level()
        if self._level_count < level:
            self._level_count = level
        new_node = ListNode(value)
        new_node.forwards = [None] * level
        update = [self._head] * level

        p = self._head
        for i in range(level - 1, -1, -1):
            while p.forwards[i] and p.forwards[i].data < value:
                p = p.forwards[i]
            update[i] = p

        for i in range(level):
            new_node.forwards[i] = update[i].forwards[i]
            update[i].forwards[i] = new_node

    def delete(self, value):
        update = [None] * self._level_count
        p = self._head
        for i in range(self._level_count - 1, -1, -1):
            while p.forwards[i] and p.forwards[i].data < value:
                p = p.forwards[i]
            update[i] = p

        if p.forwards[0] and p.forwards[0].data == value:
            for i in range(self._level_count - 1, -1, -1):
                if update[i].forwards[i] and update[i].forwards[i].data == value:
                    update[i].forwards[i] = update[i].forwards[i]

    def __random_level(self, k: float = 0.5) -> int:
        level = 1
        while random.random() < k and level < type(self)._MAX_LEVEL:
            level += 1
        return level

    def __repr__(self) -> str:
        values = []
        p = self._head
        while p.forwards[0]:
            values.append(str(p.forwards[0].data))
            p = p.forwards[0]
        return "->".join(values)


if __name__ == "__main__":
    s_list = SkipList()
    for i in range(10):
        s_list.insert(i)
    print(s_list)
    p = s_list.find(7)
    print(p.data)
    s_list.delete(3)
    print(s_list)
