# 1.单链表的插入、删除、查找操作；
# 2.链表中存储的数据类型是Int
#

# from typing import Optional


class Node:
    """连表结构的Node结点"""

    def __init__(self, data, next_node=None):
        """
        Node结点的初始化方法。
        :param data: 存储的数据
        :param next_node: 下一个结点的引用地址
        """
        self.__data = data
        self.__next = next_node

    @property
    def data(self):
        """
        :return: 当前Node结点存储的数据
        """
        return self.__data

    @data.setter
    def data(self, data):
        """
        Node结点存储数据的设置方法
        :param data: 新的存储数据
        """
        self.__data = data

    @property
    def next_node(self):
        """
        获取Node结点的next指针值
        :return: next指针数据
        """
        return self.__next

    @next_node.setter
    def next_node(self, next_node):
        """
        Node结点next指针的修改方法
        :param next_node: 新的下一个结点的引用
        """
        self.__next = next_node


class SinglyLinkedList:
    """单向链表"""

    def __init__(self):
        """单向链表的初始化方法"""
        self._head = None

    """
    插入、删除、查找：
    1、判断头结点是否为空；
    2、循环注意下一个节点是否为空；
    """
    def len(self):
        p = self._head
        length = 0
        while p:
            p = p.next_node
            length += 1
        return length

    def find_by_value(self, value):
        """
        按照数据值在单项链表中查找
        :param value: 要查找的数据
        :return: Node
        """
        node = self._head
        while node and node.data != value:
            node = node.next_node
        return node

    def find_by_index(self, index):
        """
        根据索引值返回相应的结点
        :param index: 索引值
        :return: Node
        """
        pos = 0
        node = self._head
        while node and pos != index:
            node = node.next_node
            pos += 1
        return node

    def insert_to_head(self, value):
        """
        在链表的头部插入一个数据为value的结点
        :param value:
        """
        node = Node(value)
        node.next_node = self._head
        self._head = node

    def insert_after(self, node, value):
        """
        在链表的某个指定Node节点之后插入一个存储value数据的Node节点.
        :param node: 指定的一个Node节点
        :param value: 将要存储在新Node节点中的数据
        """
        if node:
            new_node = Node(value)
            new_node.next_node = node.next_node
            node.next_node = new_node

    def insert_before(self, node, value):
        """
        在链表的某个指定Node节点之前插入一个存储value数据的Node节点.
        :param node: 指定的一个Node节点
        :param value: 将要存储在新Node节点中的数据
        """
        if node == self._head:
            self.insert_to_head(value)
            return

        if node and self._head:
            p = self._head
            not_found = False
            while p.next_node != node:
                if not p.next_node:
                    not_found = True
                    break
                else:
                    p = p.next_node

            if not not_found:
                new_node = Node(value, node)
                p.next_node = new_node

    def delete_by_node(self, node):
        """
        在链表中删除指定的node节点
        :param node: 指定的弄的节点
        """
        if node == self._head:
            self._head = node.next_node
            return

        if self._head and node:
            p = self._head
            not_found = False
            while p.next_node != node:
                if not p.next_node:
                    not_found = True
                    break
                else:
                    p = p.next_node

            if not not_found:
                p.next_node = node.next_node

    def delete_by_value(self, value):
        """
        在链表中删除存储了指定数据的Node节点
        :param value: 指定数据
        """
        if self._head.data == value:
            self._head = self._head.next_node
            return

        if self._head:
            p = self._head
            node = self._head.next_node
            not_found = False
            while node.data != value:
                if not node.next_node:
                    not_found = True
                    break
                else:
                    p = node
                    node = node.next_node

            if not not_found:
                p.next_node = node.next_node

    def delete_last_n_node(self, n):
        """
        删除链表中倒数第N个节点
        主体思路：
            设置快、慢两个指针，快指针先行，慢指针不动；当快指针跨了N步以后，快、慢指针同时往链表尾部移动，
            当快指针到达链表尾部的时候，慢指针所指向的就是链表的倒数第N个节点
        :param n:
        """
        length = self.len()

        if n == length:
            self._head = self._head.next_node
            return
        if n > length:
            print('越界')
            return
        fast = self._head
        slow = self._head
        # tmp = slow
        step = 0

        while step < n:
            fast = fast.next_node
            step += 1

        while fast.next_node:
            # tmp = slow
            fast = fast.next_node
            slow = slow.next_node

        # tmp.next_node = slow.next_node
        slow.next_node = slow.next_node.next_node

    def find_mid_node(self):
        """
        查找链表的中间节点
        主体思想:
            设置快、慢两种指针，快指针每次跨两步，慢指针每次跨一步，则当快指针到达链表尾部的时候，慢指针指向链表的中间节点
        :return: 链表的中间结点node
        """
        fast = self._head
        slow = self._head

        while fast.next_node and fast.next_node.next_node:
            fast = fast.next_node.next_node
            slow = slow.next_node

        return slow.data

    @staticmethod
    def create_node(value):
        """
        创建一个存储value值的Node节点
        :param value: 将要存储的数值
        :return: 一个新的节点
        """
        return Node(value)

    def print_all(self):
        """打印当前链表的所有的节点数据"""
        p = self._head
        if p is None:
            print("当前链表还没有数据")
            return
        while p.next_node:
            print(str(p.data) + "--->", end='')
            p = p.next_node
        print(str(p.data))

    @staticmethod
    def __reversed_with_two_node(pre, node):
        """
        翻转相邻的两个节点
        :param pre: 前一个节点
        :param node: 当前节点
        :return: (pre, node) 下一个相邻的节点的元组
        """

        tmp = node.next_node
        node.next_node = pre
        pre = node
        node = tmp
        return pre, node

    def reversed_self(self):
        """翻转链表自身."""
        if not self._head or not self._head.next_node:
            return

        pre = self._head
        node = self._head.next_node
        while node:
            pre, node = self.__reversed_with_two_node(pre, node)
        # 因为链表带有只有，每一次交换后，pre都为整体的一串的链表

        self._head.next_node = None
        self._head = pre
        # TODO:上两行需要再次思考

    def has_ring(self):
        """
        检查链表中是否有环
        主体思路：
            设置快、慢两种指针，快指针每次跨两步，慢指针每次跨一步，如果快指针没有与慢指针相遇而是顺利到达链表尾部
            说明没有环；否则，存在环
        :return: bool
        """
        fast = self._head
        slow = self._head

        while fast and fast.next_node:
            fast = fast.next_node.next_node
            slow = slow.next_node
            if fast == slow:
                return True
        return False

    @staticmethod
    def merge_two_linked_list(node1, node2):
        """
        合并两个有序链表(升序)
        :param node1: 链表A头节点
        :param node2: 链表B头节点
        :return: 合并后的链表的头节点
        """
        if node1 and node2:
            p1, p2 = node1._head, node2._head
            fake_head = Node(None)
            current = fake_head
            while p1 and p2:
                if p1.data <= p2.data:
                    current.next_node = p1
                    p1 = p1.next_node
                else:
                    current.next_node = p2
                    p2 = p2.next_node
                current = current.next_node
            current.next_node = p1 if p1 else p2
            return fake_head.next_node
        return node1 or node2

    def delete_all_target_value(self, value):
        """
        删除链表中所有的data == value的节点
        :param value:
        :return:
        """
        if self._head:
            p = self._head
            n = p.next_node
            while n:
                if p.data == value:
                    p = p.next_node
                    self._head = p
                elif n.data == value:
                    p.next_node = n.next_node
                else:
                    p = n
                n = n.next_node


def test_list():
    linked_list = SinglyLinkedList()
    linked_list.insert_to_head(5)
    linked_list.insert_to_head(6)
    linked_list.insert_to_head(9)
    linked_list.insert_to_head(8)
    linked_list.insert_to_head(3)
    linked_list.insert_to_head(4)
    linked_list.insert_to_head(1)
    linked_list.insert_to_head(7)
    linked_list.print_all()
    node = linked_list.find_by_value(3)
    linked_list.insert_after(node, 15)
    linked_list.insert_before(node, 13)
    node_p = linked_list.find_by_value(11)
    print('1:', node_p)
    linked_list.insert_before(node_p, 3)
    linked_list.print_all()
    print('2:', linked_list.has_ring())
    assert linked_list.find_by_value(6).data == 6
    assert linked_list.find_by_value(7) is not None
    linked_list.print_all()
    linked_list.reversed_self()
    linked_list.print_all()
    print('3 : length:', linked_list.len())
    linked_list.delete_last_n_node(6)
    linked_list.print_all()
    linked_list.delete_by_value(19)
    linked_list.print_all()
    linked_list.delete_by_value(13)
    linked_list.print_all()
    print('123')
    linked_list.delete_by_node(node)
    linked_list.print_all()
    linked_list.delete_by_node(node_p)
    linked_list.print_all()
    nodeb = Node(21)
    linked_list.delete_by_node(nodeb)
    linked_list.print_all()
    print('4:', linked_list.find_mid_node())
    linked_list1 = SinglyLinkedList()
    linked_list1.insert_to_head(19)
    linked_list1.insert_to_head(15)
    linked_list1.insert_to_head(13)
    linked_list1.insert_to_head(9)
    linked_list1.insert_to_head(5)
    linked_list1.insert_to_head(3)
    linked_list1.insert_to_head(1)
    linked_list2 = SinglyLinkedList()
    linked_list2.insert_to_head(18)
    linked_list2.insert_to_head(16)
    linked_list2.insert_to_head(14)
    linked_list2.insert_to_head(11)
    linked_list2.insert_to_head(10)
    linked_list1.print_all()
    linked_list2.print_all()
    linked_list1.merge_two_linked_list(linked_list1, linked_list2)
    linked_list1.print_all()
    linked_list1.insert_to_head(18)
    linked_list1.insert_to_head(13)
    linked_list1.insert_to_head(3)
    linked_list1.insert_to_head(5)
    linked_list1.insert_to_head(11)
    linked_list1.insert_to_head(18)
    linked_list1.insert_to_head(18)
    linked_list1.insert_to_head(18)
    linked_list1.print_all()
    linked_list1.delete_all_target_value(18)
    linked_list1.print_all()


if __name__ == "__main__":
    test_list()
