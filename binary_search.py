"""
二分查找：可以依赖循环和递归实现
"""

from typing import List


def binary_search(a: int, val: List[int]):
    """递归"""
    if a < val[0] or a > val[-1]:
        print("None")
        return None
    mid = len(val) // 2
    if a == val[mid]:
        print(val[mid])
        return val[mid]
    elif a > val[mid]:
        binary_search(a, val[mid + 1:])
    else:
        binary_search(a, val[:mid])


def binary_search2(a: int, val: List[int]):
    """循环"""
    low, high = 0, len(val) - 1
    while low <= high:
        """
        1、mid = (low+high) // 2 , 如果low 和high比较大的话，容易出现溢出报错
        2、性能极致优化的话，mid = low + ((high-low)>>1)
            位运算比除法运算快很多
        3、low、high更新，如果直接写为low=mid \ high=mid,可能会发生死循环
        """
        mid = low + (high - low) // 2
        if a == val[mid]:
            print(val[mid])
            return val[mid]
        elif a > val[mid]:
            low = mid + 1
        else:
            high = mid - 1
    print('None')
    return None


def test():
    array = [-1, 2, 4, 5, 6, 6, 7, 8, 10]
    binary_search(8, array)
    binary_search2(8, array)
    binary_search(3, array)
    binary_search2(3, array)
    binary_search(11, array)
    binary_search2(11, array)
    binary_search(-3, array)
    binary_search2(-3, array)
    binary_search(2, array)
    binary_search2(2, array)
    binary_search(9, array)
    binary_search2(9, array)


if __name__ == "__main__":
    test()
