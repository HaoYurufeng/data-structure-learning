"""
对比各种排序的效率
另：
Python默认最大递归深度为1000
，可以通过以下语句修改，但是最多递归到3220左右
所以近乎有序的数组递归排序会报错
import sys
sys.setrecursionlimit(100000)
"""
import numpy as np
import timeit
import random


class Sort(object):
    @staticmethod
    def random_array(n):
        return list(np.random.randint(1000, size=n))

    def random_nearly_order_array(self, n, swaptimes):
        nums = self.random_array(n)
        nums.sort()
        for i in range(swaptimes):
            x = random.randint(0, n)
            y = random.randint(0, n)
            nums[x], nums[y] = nums[y], nums[x]
        return nums

    @staticmethod
    def bubble_sort(nums_list):
        nums = nums_list.copy()
        start_time = timeit.default_timer()
        length = len(nums)
        for i in range(length):
            for j in range(length-i-1):
                if nums[j] > nums[j+1]:
                    nums[j], nums[j+1] = nums[j+1], nums[j]
        end_time = timeit.default_timer()
        print('冒泡排序的时间是：', (end_time - start_time) * 100000)
        # print(nums)
        print('=====================================\r\t')

    @staticmethod
    def selection_sort(nums_list):
        nums = nums_list.copy()
        start_time = timeit.default_timer()
        n = len(nums)
        for i in range(0, n-1):
            min_index = i
            for j in range(i+1, n):
                if nums[j] < nums[min_index]:
                    min_index = j
            nums[min_index], nums[i] = nums[i], nums[min_index]
        end_time = timeit.default_timer()
        print('选择排序的时间是：', (end_time - start_time)*100000)
        # print(nums)
        print('=====================================\r\t')

    @staticmethod
    def selection_sort_pre(nums_list):
        nums = nums_list.copy()
        start_time = timeit.default_timer()
        n = len(nums)
        for i in range(0, n):
            nums1 = nums[i:n]
            min_num = min(nums1)
            min_index = nums1.index(min_num) + i
            nums[min_index], nums[i] = nums[i], min_num
        end_time = timeit.default_timer()
        print('优化后选择排序的时间是：', (end_time - start_time)*100000)
        # print(nums)
        print('=====================================\r\t')

    @staticmethod
    def sort(nums_list):
        nums = nums_list.copy()
        start_time = timeit.default_timer()
        n = len(nums)
        for i in range(n):
            nums1 = nums[i:n]
            min_value = min(nums1)
            min_index = nums1.index(min_value) + i
            nums.insert(i, min_value)
            nums.pop(min_index+1)
        end_time = timeit.default_timer()
        print('优化后选择排序的时间是：', (end_time - start_time) * 100000)
        # print(nums)
        print('=====================================\r\t')

    @staticmethod
    def insertion_sort(nums_list):
        nums = nums_list.copy()
        start_time = timeit.default_timer()
        for i in range(1, len(nums)):
            j = i
            while j > 0 and nums[j] < nums[j-1]:
                nums[j], nums[j-1] = nums[j-1], nums[j]
                j = j - 1
        end_time = timeit.default_timer()
        print('插入排序的时间是：', (end_time - start_time)*100000)
        # print(nums)
        print('=====================================\r\t')

    @staticmethod
    def insertion_sort_pre(nums_list):
        nums = nums_list.copy()
        start_time = timeit.default_timer()
        for i in range(1, len(nums)):
            j = i
            now = nums[j]
            while j > 0 and now < nums[j-1]:
                nums[j] = nums[j-1]
                j = j - 1
            nums[j] = now
        end_time = timeit.default_timer()
        print('优化后插入排序的时间是：', (end_time - start_time)*100000)
        # print(nums)
        print('=====================================\r\t')
        return nums

    @staticmethod
    def insertion_sort_pre_more(nums_list):
        nums = nums_list.copy()
        start_time = timeit.default_timer()
        for i in range(1, len(nums)):
            j = i
            now = nums[j]
            while j > 0 and now < nums[j-1]:
                j = j - 1
            nums.insert(j, now)
            nums.pop(i+1)
        end_time = timeit.default_timer()
        print('进一步优化后插入排序的时间是：', (end_time - start_time)*100000)
        # print(nums)
        print('=====================================\r\t')

    @staticmethod
    def insertion_sort2(nums_list):
        """
        思路（升序）：按顺序遍历每一个元素，与自己之前的元素做比较,若小，则插入元素，所有元素往后移动
        """
        a = nums_list.copy()
        start_time = timeit.default_timer()
        length = len(a)
        if length > 1:
            for i in range(1, length):
                value = a[i]
                j = i - 1
                while j >= 0 and a[j] > value:
                    # a[j + 1] = a[j]
                    j -= 1
                # a[j + 1] = value
                a.insert(j+1, value)
                a.pop(i+1)
        end_time = timeit.default_timer()
        print('插入排序再次优化运行时间：', (end_time - start_time) * 100000)
        # print(a)
        print('=====================================\r\t')

    @staticmethod
    def shell_sort(nums_list):
        nums = nums_list.copy()
        start_time = timeit.default_timer()
        length = len(nums)
        gap = length // 2
        while gap > 0:
            for i in range(gap, length):
                j = i
                while j - gap >= 0:
                    if nums[j] < nums[j-gap]:
                        nums[j], nums[j - gap] = nums[j-gap], nums[j]
                        j -= gap
                    else:
                        break
            gap //= 2
        end_time = timeit.default_timer()
        print('希尔排序的时间是：', (end_time - start_time) * 100000)
        # print(nums)
        print('=====================================\r\t')

    def __merge_sort(self, nums):
        """
        归并排序：递归
        对[l, ……, r]闭区间
        """
        if len(nums) > 1:
            mid = len(nums) // 2
            left = self.__merge_sort(nums[:mid])
            right = self.__merge_sort(nums[mid:])
            return self.__merge(left, right)
        else:
            return nums

    @staticmethod
    def __merge(left, right):
        num = []
        i = j = 0
        while i < len(left) and j < len(right):
            if left[i] < right[j]:
                num.append(left[i])
                i = i + 1
            else:
                num.append(right[j])
                j = j + 1

        if i == len(left):
            for n in right[j:]:
                num.append(n)
        else:
            for n in left[i:]:
                num.append(n)
        return num

    def merge_sort(self, nums_list):
        """归并排序：递归"""
        nums = nums_list.copy()
        start_time = timeit.default_timer()
        self.__merge_sort(nums)
        end_time = timeit.default_timer()
        print('归并排序的时间是：', (end_time - start_time) * 100000)
        # print(self.__merge_sort(nums))
        print('=====================================\r\t')

    def __merge_sort_pre(self, nums):
        """
        优化后的归并排序：递归
        对[l, ……, r]闭区间
        """
        if len(nums) <= 15:
            return self.insertion_sort_pre(nums)
        if len(nums) > 1:
            mid = len(nums) // 2
            left = self.__merge_sort(nums[:mid])
            right = self.__merge_sort(nums[mid:])
            if left[-1] < right[0]:
                return self.__merge(left, right)
            return left + right
        else:
            return nums

    def merge_sort_pre(self, nums_list):
        """归并排序：递归"""
        nums = nums_list.copy()
        start_time = timeit.default_timer()
        self.__merge_sort_pre(nums)
        end_time = timeit.default_timer()
        print('优化后的归并排序的时间是：', (end_time - start_time) * 100000)
        # print(self.__merge_sort(nums))
        print('=====================================\r\t')

    def merge_sort_bu(self, nums_list):
        """自底向上的归并排序：循环"""
        nums = nums_list.copy()
        start_time = timeit.default_timer()
        sz = 1
        length = len(nums)
        while sz < length:
            for i in range(0, length - sz, 2*sz):
                nums[i: min(i + 2*sz, length)] = self.__merge(nums[i: i + sz], nums[i + sz: min(i + 2*sz, length)])
            sz += sz
        end_time = timeit.default_timer()
        print('自底向上的归并排序的时间是：', (end_time - start_time) * 100000)
        # print(nums)
        print('=====================================\r\t')

    @staticmethod
    def __partition(nums):
        n = nums[0]
        p = 0
        for i in range(1, len(nums)):
            if nums[i] <= n:
                nums[i], nums[p + 1] = nums[p + 1], nums[i]
                p = p + 1
        nums[p], nums[0] = nums[0], nums[p]
        return p

    def __quick_sort(self, nums):
        p_index = self.__partition(nums)
        if len(nums) <= 2 or (len(nums) == 3 and p_index == 1):
            return nums
        elif p_index in (len(nums) - 1, len(nums) - 2):  # 基准点是最后两个元素，基准点后的元素不需要再次排序
            return self.__quick_sort(nums[:p_index]) + nums[p_index:]
        elif p_index in (0, 1):
            return nums[:p_index + 1] + self.__quick_sort(nums[p_index + 1:])
        else:
            return self.__quick_sort(nums[:p_index]) + [nums[p_index]] + self.__quick_sort(nums[p_index + 1:])

    def quick_sort(self, nums_list):
        """快速排序"""
        nums = nums_list.copy()
        start_time = timeit.default_timer()
        self.__quick_sort(nums)
        end_time = timeit.default_timer()
        print('快速排序的时间是：', (end_time - start_time) * 100000)
        # print(self.__quick_sort(nums))
        print('=====================================\r\t')

    def __quick_sort_pre(self, nums):
        if len(nums) <= 50:
            return self.insertion_sort_pre(nums)
        else:
            p = self.__partition(nums)
            if len(nums) == 1:
                return nums
            elif p == len(nums)-1:
                return self.__quick_sort(nums[:p]) + [nums[p]]
            elif p == 0:
                return [nums[p]] + self.__quick_sort(nums[p+1:])
            else:
                return self.__quick_sort(nums[:p]) + [nums[p]] + self.__quick_sort(nums[p+1:])

    def quick_sort_pre(self, nums_list):
        """快速排序"""
        nums = nums_list.copy()
        start_time = timeit.default_timer()
        self.__quick_sort_pre(nums)
        end_time = timeit.default_timer()
        print('优化后的快速排序的时间是：', (end_time - start_time) * 100000)
        # print(self.__quick_sort(nums))
        print('=====================================\r\t')


def test():
    v = Sort()
    nums = v.random_array(10000)
    nums1 = v.random_nearly_order_array(10000, 10)
    # array = [5, 6, -1, 4, 2, 8, 10, 7, 6]
    # # print('原数组是：\r', nums1)
    v.bubble_sort(nums)
    v.selection_sort(nums)
    v.selection_sort_pre(nums)
    v.sort(nums)
    v.insertion_sort(nums)
    v.insertion_sort_pre(nums)
    v.insertion_sort_pre_more(nums)
    v.insertion_sort2(nums)
    v.merge_sort(nums)
    v.merge_sort_pre(nums)
    v.merge_sort_bu(nums)
    v.quick_sort(nums)
    v.quick_sort_pre(nums)
    v.shell_sort(nums)

    print('近乎有序的数组：')
    v.bubble_sort(nums1)
    v.selection_sort(nums1)
    v.selection_sort_pre(nums1)
    v.sort(nums1)
    v.insertion_sort(nums1)
    v.insertion_sort_pre(nums1)
    v.insertion_sort_pre_more(nums1)
    v.insertion_sort2(nums1)
    v.merge_sort(nums1)
    v.merge_sort_pre(nums1)
    v.merge_sort_bu(nums1)
    v.quick_sort(nums1)
    v.quick_sort_pre(nums1)
    v.shell_sort(nums1)


if __name__ == "__main__":
    test()
