from typing import List
import numpy as np
import time
import copy


def insertion_sort(a: List[int]):
    start_time = time.time()
    length = len(a)
    for i in range(1, length):
        value = a[i]
        j = i-1
        while j >= 0 and a[j] > value:
            a[j+1] = a[j]
            j -= 1
        a[j+1] = value
    end_time = time.time()
    print('插入排序运行时间：', (end_time - start_time) * 10000)


def shell_sort(a: List[int]):
    start_time = time.time()
    length = len(a)
    gap = length // 2
    while gap > 0:
        for j in range(gap, length):
            i = j
            while i - gap >= 0:
                if a[i] < a[i-gap]:
                    a[i], a[i - gap] = a[i - gap], a[i]
                    i -= gap
                else:
                    break
        gap //= 2
    end_time = time.time()
    print('希尔排序运行时间：', (end_time - start_time) * 10000)


if __name__ == "__main__":
    a = np.random.randint(0, 1000, 1000)
    array = [5, 6, -1, 4, 2, 8, 10, 7, 6]
    array_b = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 1, 1, 1, 1, 1, 1, 1, 2,
               1, 1, 1, 1, 1, 1, 1, 1, 91, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1,
               1, 5]*20
    a1 = copy.deepcopy(a)
    a2 = copy.deepcopy(a)
    ar1 = copy.deepcopy(array)
    ar2 = copy.deepcopy(array)
    ab1 = copy.deepcopy(array_b)
    ab2 = copy.deepcopy(array_b)
    insertion_sort(a1)
    shell_sort(a2)
    insertion_sort(ar1)
    shell_sort(ar2)
    # print(ar2)
    insertion_sort(ab1)
    shell_sort(ab2)
